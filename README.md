## Project Summary
This is a basic serialize/deserializer tool. It expects name, phone
number, and address data as input and can output that data in a
human-friendly format.

Supported input formats:

- csv
- json

Supported output formats:

- html
- yaml

The rest of this file will go over some basic information on how to
test, run, and extend this tool.

Additional Notes:

- A technical brief for this project can be viewed in `PROJECT_BRIEF.md`
- Part of the requirement of this project is to "describe any problems
  or pitfalls you expected, and how you overcome them". This information
  is available in `PROJECT_LOG.md`.


## How to run the command-line interface
1. Make sure that `{root}/src` is located in your PYTHONPATH
2. Install all of the packages in `requirements.txt`
With pip installed and access to Internet, this can be done by running this on command-line:


```bash
pip install -r {root}/requirements.txt
```

3. Next is to run the command-line interfase, like this:

```bash
python -m data_display --help
```

There are two commands, `convert` and `ls`. 

`ls` displays all of the available reader and writer formats that are supported:

```bash
python -m data_display ls
```

`convert` changes one format into another format. See examples of it in the 
[Examples on converting files](#Examples-on-converting-files) section.


## How to run the unittests
1. Install [tox](https://pypi.org/project/tox)
2. cd to this repository's root directory and run `tox`

```bash
cd {root} && tox
```


## Environment Variables
`DATA_DISPLAY_DISABLE_LOGGER` `(default: 1)`

- This environment variable is added to this repository's unittests to avoid
  logging output from being displayed with the unittest's results. Set to 0 to
  re-enable logging on unittests.

`DATA_DISPLAY_READER_SEARCH_ORDER` `(default: "csv,json")`

- If not reader adapter type (example: `data_display convert --reader-type csv`)
  is given then adapters will be searched for in the order defined by this
  environment variable.


## Examples on converting files
Convert from CSV to HTML

```bash
python -m data_display convert {root}/example/input.csv html --reader-options '{"encoding": "utf8"}' --output-location /tmp/location.html
```

Convert from CSV to YAML

```bash
python -m data_display convert {root}/example/input.csv yaml --reader-options '{"encoding": "utf8"}' --output-location /tmp/location.yaml
```

Convert from JSON to HTML

```bash
python -m data_display convert {root}/example/input.json html --output-location /tmp/location.html
```

Convert from JSON to YAML

```bash
python -m data_display convert {root}/example/input.json yaml --output-location /tmp/location.yaml
```


Convert from one type to another and change output columns

```bash
python -m data_display convert {root}/example/input.json yaml --reader-options '{"phone_column_index": 2, "address_column_index": 1}' --output-location /tmp/location.yaml
```
