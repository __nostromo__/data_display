#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""A set of public functions and classes that can be used outside of this package.

This module exists to help ease any issues with backwards/forwards compatibility.

"""


# IMPORT LOCAL LIBRARIES
from .core.converter import convert
from .core.registrar import add_adapter
from .core.registrar import remove_adapter
from .core.registrar import get_reader_adapter
from .core.registrar import get_writer_adapter
from .core.registrar import get_all_reader_adapters
from .core.registrar import get_all_writer_adapters
from .adapters.base_reader import AbstractReader
from .adapters.base_writer import AbstractWriter

__all__ = [
    "AbstractReader",
    "AbstractWriter",
    "add_adapter",
    "convert",
    "get_all_reader_adapter",
    "get_all_reader_adapters",
    "get_writer_adapter",
    "get_writer_adapters",
    "remove_adapter",
]
