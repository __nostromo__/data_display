#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""An adapter which can be used to convert data into some human-readable format."""

# IMPORT STANDARD LIBRARIES
import abc

# IMPORT THIRD-PARTY LIBRARIES
import six


@six.add_metaclass(abc.ABCMeta)
class AbstractWriter(object):

    """A generic class that can read some format and deserialize it.

    This class does nothing on its own and is meant to be subclassed and extended.

    """

    def __init__(self, options=None):
        """Store any optional metadata.

        Args:
            options (dict[str], optional):
                This is extra data that you can use to control the behavior of
                this instance. Default is None.

        """
        super(AbstractWriter, self).__init__()

        if not options:
            options = dict()

        self.options = options

    @abc.abstractproperty
    def requires_disk(self):
        """bool: If this class needs to write somewhere to-file."""
        return False

    def get_content_footer(self, lines):
        """list[list[str]]: Get text that will be added after all lines are processed."""
        return []

    def get_content_header(self, lines):
        """list[list[str]]: Get text that will be added before all lines are processed."""
        return []

    def get_modified_line(self, line):
        """Get the lines of data.

        This method lets you do some final text manipulation.

        Returns:
            list[list[str]]: The processed lines.

        """
        return [line]

    def prepare(self, lines):
        """Convert the given lines of data into this class's format.

        This method may write to disk, stdout, or perform any other output operation.

        Args:
            lines (list[list[str]]):
                The rows and columns of information to convert into this
                instance's format type.

        Returns:
            list[list[str]]:
                The rows and columns that are ready to be used for writing to
                this instance's format.

        """
        output = self.get_content_header(lines)

        for line in lines:
            output.extend(self.get_modified_line(line))

        output.extend(self.get_content_footer(lines))

        return output

    @abc.abstractmethod
    def write(self, lines):
        """Write the given `lines` to some output (stream, file, etc.)."""
        pass


class FileWriter(AbstractWriter):

    """A version of `AbstractWriter` that writes to the user's disk."""

    requires_disk = True

    def __init__(self, output_location, options=None):
        """Store the given file location and any other options for writing.

        Args:
            output_location (str):
                The path on-disk where this instance will write content to.
            options (dict[str], optional):
                This is extra data that you can use to control the behavior of
                this instance. Default is None.

        """
        super(FileWriter, self).__init__(options=options)
        self._output_location = output_location

    def get_output_location(self):
        """str: The location on-disk that will be used to writing."""
        return self._output_location
