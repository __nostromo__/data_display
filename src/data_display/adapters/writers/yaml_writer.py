#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The basic adapter which is used to write to YAML."""

# IMPORT STANDARD LIBRARIES
import logging
import tempfile

# IMPORT THIRD-PARTY LIBRARIES
import yaml

# IMPORT LOCAL LIBRARIES
from .. import base_writer

LOGGER = logging.getLogger("data_display.yaml_writer")


class Writer(base_writer.FileWriter):

    """A class that reads Python objects and converts them to a standard YAML file."""

    description = (
        "Write a list of names, addresses, and phone numbers to a YAML file. "
        'Add "output_location" as an option to write to disk.'
    )

    def get_modified_line(self, line):
        """list[list[str]]: Delete any leading and trailing whitespace between line items."""
        return [[item.strip() for item in line]]

    def write(self, lines):
        """Write the given `lines` to a YAML file."""
        path = self.get_output_location()

        if not path:
            path = tempfile.NamedTemporaryFile(suffix=".yaml").name

        with open(path, "w") as handler:
            yaml.dump(lines, handler)

        LOGGER.info('YAML "%s" was succcessfully written to disk.', path)
