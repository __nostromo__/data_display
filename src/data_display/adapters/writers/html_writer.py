#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The basic adapter which is used to write to HTML."""

# IMPORT STANDARD LIBRARIES
import sys
import logging
import tempfile
import textwrap

# IMPORT LOCAL LIBRARIES
from .. import base_writer

LOGGER = logging.getLogger("data_display.html_writer")


class Writer(base_writer.FileWriter):

    """A class that reads Python objects and converts them to a standard HTML file."""

    description = (
        "Write a list of names, addresses, and phone numbers as a webpage table. "
        'Add "output_location" as an option to write to disk.'
    )

    def get_content_header(self, lines):
        """list[str]: Create what will be the top of the HTML file."""
        return [
            textwrap.dedent(
                """\
                <html>
                <header><title>People Summary</title></header>
                <body>
                <table style="width:100%">
                """
            )
        ]

    def get_content_footer(self, lines):
        """list[str]: Close the tags that were created in `get_content_header`."""
        return ["</table>\n</body>\n</html>"]

    def get_modified_line(self, line):
        """list[str]: Close each line in a HTML table row."""
        name_parts = line[0].split()

        try:
            first_name = name_parts[0]
        except IndexError:
            first_name = ""

        last_name = " ".join(name_parts[1:])

        template = textwrap.dedent(
            u"""\
            <tr>
                <th>{first_name}</th>
                <th>{last_name}</th>
                <th>{phone_number}</th>
                <th>{address}</th>
            </tr>
            """
        )

        template = template.format(
            first_name=first_name,
            last_name=last_name,
            phone_number=line[1],
            address=line[2],
        )

        return [template]

    def write(self, lines):
        """Write the given `lines` to a HTML file."""
        path = self.get_output_location()

        if not path:
            path = tempfile.NamedTemporaryFile(suffix=".html").name

        with open(path, "w") as handler:
            if sys.version_info >= (3, 0):
                handler.write("\n".join(lines))
            else:
                LOGGER.debug("Output lines were given a utf-8 encoding")
                handler.write("\n".join(lines).encode("utf-8"))

        LOGGER.info('HTML "%s" was succcessfully written to disk.', path)
