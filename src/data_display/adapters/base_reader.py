#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""An adapter which can be used to read, parse, and filter data from some format."""

# IMPORT STANDARD LIBRARIES
import abc
import sys
import logging

# IMPORT THIRD-PARTY LIBRARIES
import six


LOGGER = logging.getLogger('data_display.base_reader')


@six.add_metaclass(abc.ABCMeta)
class AbstractReader(object):

    """A generic class that can read some format and deserialize it.

    This class does nothing on its own and is meant to be subclassed and extended.

    """

    def __init__(self, handle, options=None):
        """Check if `handle` is parseable and store any optional metadata.

        Args:
            handle:
                Some reference to the data that will be parsed.
                It could be raw data, a path to a file, or some other kind of
                descriptor.
            options (dict[str], optional):
                This is extra data that you can use to control the behavior of
                this instance. Default is None.

        """
        super(AbstractReader, self).__init__()

        if not options:
            options = dict()

        self.options = options
        self.handle = handle

    @abc.abstractmethod
    def is_supported(self, obj):
        """bool: Check if `obj` is meant to be parsed by this object."""
        return False

    @abc.abstractmethod
    def deserialize(self):
        """list[list[str]]: Get the data from this class's stored arguments."""
        return []


class LinesOfLinesReader(AbstractReader):

    """A generic reader class which can read a 2D file format of names, addresses, and phone numbers.

    Inherit this class in a subclass and implement `_get_reader_handle()`
    method to use this base-class.

    """

    def __init__(self, path, options=None):
        """Check if `path` is a valid file and, if so, store it and any other options.

        Args:
            path (str):
                The absolute path to some file on-disk.
            options (dict[str], optional):
                This is extra data that you can use to control the behavior of
                this instance. Default is None.

        Raises:
            ValueError:
                If no column information could be found in the given `path` file.

        """
        if not options:
            options = dict()

        super(LinesOfLinesReader, self).__init__(path, options=options)

        self.address_column_index = self.options.get("address_column_index", -1)
        self.name_column_index = self.options.get("name_column_index", -1)
        self.phone_column_index = self.options.get("phone_column_index", -1)

        header_line = self._find_header_line(self._get_reader_handle())

        if self.address_column_index == -1:
            self.address_column_index = self._find_address_column(header_line)

        if self.name_column_index == -1:
            self.name_column_index = self._find_name_column(header_line)

        if self.phone_column_index == -1:
            self.phone_column_index = self._find_phone_column(header_line)

        if -1 in (
                self.address_column_index,
                self.name_column_index,
                self.phone_column_index,
        ):
            raise ValueError('Path "{path}" has no header.'.format(path=path))

    def __find_header_column(self, line, label):
        prefix = self.options.get("header_prefix", "--")

        full_label = "{prefix}{label}".format(prefix=prefix, label=label)
        try:
            return line.index(full_label)
        except ValueError:
            LOGGER.exception('Label "%s" could not be found in line, "%s"', full_label, line)
            return -1

    def _is_header(self, line):
        """bool: If this line represents the top of file's data."""
        prefix = self.options.get("header_prefix", "--")
        return line and all(item.startswith(prefix) for item in line)

    def _find_address_column(self, line):
        """Find the index from a header line that represent's a home address.

        Args:
            line (list[str]): The header-line to check.

        Returns:
            int: The found column index or -1, if not found.

        """
        return self.__find_header_column(line, "Address")

    def _find_header_line(self, lines):
        """list[str]: Get the header data of some file's lines, if it can be found."""
        for line in lines:
            if self._is_header(line):
                return line

        return []

    def _find_name_column(self, line):
        """Find the index from a header line that represent's a user's name.

        Args:
            line (list[str]): The header-line to check.

        Returns:
            int: The found column index or -1, if not found.

        """
        return self.__find_header_column(line, "Name")

    def _find_phone_column(self, line):
        """Find the index from a header line that represent's a phone number.

        Args:
            line (list[str]): The header-line to check.

        Returns:
            int: The found column index or -1, if not found.

        """
        return self.__find_header_column(line, "Phone Number")

    @abc.abstractmethod
    def _get_reader_handle(self):
        return
        yield

    def deserialize(self):
        """Convert this instance's file into Python lines.

        Returns:
            list[str]:
                If "clean" is True and the found line is empty, it is not
                included in the result.

                If "include_header" is True then it is added to the returned
                result. Otherwise, only data-lines are included.

                If this instance's properties are:

                {"clean": False, "include_header": True}

                Then every line of the will be returned as a list of Python strings.

        """
        def _add_line(line, lines):
            """Add `line` directly into `lines` according to this instance's
            properties."""
            name = line[self.name_column_index]
            address = line[self.address_column_index]
            phone_column_index = line[self.phone_column_index]
            lines.append([name, phone_column_index, address])

        lines = []

        include_header = self.options.get("include_header", False)
        clean = self.options.get("clean", True)
        _found_header = False

        encoding = self.options.get("encoding", "utf8")
        encoding_enabled = "encoding" in self.options and sys.version_info < (3, 0)
        if encoding_enabled:
            LOGGER.info('Reader encoding "%s" will be applied to all lines', encoding)

        for index, line in enumerate(self._get_reader_handle()):
            if clean and not line or all(not item for item in line):
                continue

            if encoding_enabled:
                line = [item.decode(encoding) for item in line]


            if _found_header:
                _add_line(line, lines)
                continue

            if self._is_header(line):
                LOGGER.debug('Header found on line "%s"', index)
                _found_header = True

                if include_header:
                    _add_line(line, lines)

                continue

        return lines
