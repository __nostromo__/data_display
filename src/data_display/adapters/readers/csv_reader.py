#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The main class that reads a CSV file and converts it to Python objects."""

# IMPORT STANDARD LIBRARIES
import csv

# IMPORT LOCAL LIBRARIES
from .. import base_reader


class Reader(base_reader.LinesOfLinesReader):

    """A converter which reads a CSV file and returns Python objects."""

    description = 'Read files that are separated by a character (like "|") and newlines'

    def _get_reader_handle(self):
        # '''list[str]: Get every line of this .'''
        with open(self.handle, "r") as handle:
            reader = csv.reader(handle, delimiter=self.options.get("delimiter", "|"))

            for line in reader:
                yield line

    @classmethod
    def is_supported(cls, obj):
        """bool: Check if the given file path points to a CSV file."""
        return obj.lower().endswith(".csv")
