#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The main class that reads a JSON file and converts it to Python objects."""

# IMPORT STANDARD LIBRARIES
import json

# IMPORT LOCAL LIBRARIES
from .. import base_reader


class Reader(base_reader.LinesOfLinesReader):

    """A converter which reads a JSON file and returns Python objects."""

    description = "Convert Javascript Object Notation files into Python objects"

    def _get_reader_handle(self):
        # '''list[str]: Get every line of this .'''
        with open(self.handle, "r") as handle:
            lines = json.load(handle)

            for line in lines:
                yield line

    @classmethod
    def is_supported(cls, obj):
        """bool: Check if the given file path points to a JSON file."""
        return obj.lower().endswith(".json")
