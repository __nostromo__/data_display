#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Run this package's command-line interface if it is called directly.

Example:
    >>> python -m data_display

"""

# IMPORT LOCAL LIBRARIES
from . import cli

if __name__ == "__main__":
    cli.main()
