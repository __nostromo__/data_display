#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The main command-line utility for this package."""

# IMPORT FUTURE LIBRARIES
from __future__ import print_function

# IMPORT STANDARD LIBRARIES
import os
import sys
import json
import logging
import argparse
import operator
import textwrap

# IMPORT LOCAL LIBRARIES
from .core import converter
from .core import registrar

LOGGER = logging.getLogger("data_display.cli")


def _convert_data(arguments):
    """Unpack a set of command-line arguments and convert from one format to another.

    Args:
        arguments (`argparse.Namespace`):
            A wrapper for the user's command-line arguments.

    """
    def _unpack_json(data):
        if not data:
            return dict()

        try:
            return json.loads(data)
        except ValueError:
            raise ValueError(
                'Options "{data}" is not a value JSON object.'.format(data=data)
            )

    reader_options = _unpack_json(arguments.reader_options)
    writer_options = _unpack_json(arguments.writer_options)

    LOGGER.debug('Got reader options "%s"', reader_options)
    LOGGER.debug('Got writer options "%s"', writer_options)

    converter.convert(
        arguments.input,
        arguments.output_type,
        reader_type=arguments.reader_type,
        reader_options=reader_options,
        writer_options=writer_options,
        output_location=arguments.output_location,
    )


def _list_all_adapters(arguments=None):  # pylint: disable=unused-argument
    """Print every reader/write format type to the user."""
    def format_adapter_list(adapters):
        """list[str]: Get printable lines for every adapter in a list of adapters."""
        if not adapters:
            return ["? None found"]

        lines = []
        for name, adapter in sorted(adapters.items(), key=operator.itemgetter(0)):
            if hasattr(adapter, "description"):
                lines.append(
                    " - {name} - {adapter.description}".format(
                        name=name, adapter=adapter
                    )
                )
            else:
                lines.append(" - {name}".format(name=name))

        return lines

    print("# All adapters")
    print("## Readers")
    print("\n".join(format_adapter_list(registrar.get_all_reader_adapters())))
    print("## Writers")
    print("\n".join(format_adapter_list(registrar.get_all_writer_adapters())))


def main():
    """Parse and evaluate the user's given command-line arguments.

    This function has the following commands:

    - convert
    - ls

    ls is used to query information about your runtime environment (what
    formats are supported, etc).

    convert is used to change from one format to another.

    """
    parser = argparse.ArgumentParser(
        description="An example command-line data-serializer tool for Animal Logic",
        version="0.3.0",
    )

    help_text = (
        "A set of Python files which can be used to add, override, "
        "or delete existing reader/writer adapters. "
        'Each file must have a `main()` function and separated by a "{separator}"'
    ).format(separator=os.pathsep)

    parser.add_argument("-a", "--adapter-files", default="", help=help_text)

    subparsers = parser.add_subparsers(
        help="The actions that can be used.", dest="subparsers"
    )

    convert_parser = subparsers.add_parser(
        "convert", help="Change a supported data format into a a human-readable format."
    )
    convert_parser.set_defaults(execute=_convert_data)

    convert_parser.add_argument(
        "input", help="An absolute path to a file or some object to read and parse."
    )

    convert_parser.add_argument(
        "output_type",
        choices=sorted(registrar.get_all_writer_adapters()),
        help="The format that `input` will be changed into."
    )

    help_text = textwrap.dedent(
        """\
        The format which will be used to read and parse the given input file.

        If no format is given then the format will be "guessed"
        automatically.
        """
    )

    convert_parser.add_argument(
        "-t",
        "--reader-type",
        choices=sorted(registrar.get_all_reader_adapters()),
        default="",
        help=help_text)

    convert_parser.add_argument(
        '-o',
        '--output-location',
        default='',
        help='If path on-disk where `output_type` will be written to, if required.',
        nargs='?',
    )

    convert_parser.add_argument(
        "-r",
        "--reader-options",
        default="",
        help="An optional JSON dict that can be used sent to the reader formatter class.",
    )

    convert_parser.add_argument(
        "-w",
        "--writer-options",
        default="",
        help="An optional JSON dict that can be used sent to the writer formatter class.",
    )

    list_parser = subparsers.add_parser(
        "ls", help="Show all supported formats for reading and writing."
    )

    list_parser.set_defaults(execute=_list_all_adapters)

    arguments = parser.parse_args()

    adapter_files = []

    if arguments.adapter_files:
        adapter_files = arguments.adapter_files.split(os.pathsep)

    try:
        converter.process_adapter_files(adapter_files)
    except RuntimeError as error:
        LOGGER.exception(
            'data_display failed to execute "%s" adapter files.', adapter_files
        )
        sys.exit(str(error))

    arguments.execute(arguments)


if __name__ == "__main__":
    main()
