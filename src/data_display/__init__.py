#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Initialize this package's logger."""

# IMPORT STANDARD LIBRARIES
import logging
import sys


def __initialize_package_logger():
    """Create a simple output logger.

    Normally this logger would be given a name (instead of adding it to the
    root logger like I am here).

    It would also log to a database, not to the terminal.

    """
    handlers = [logging.StreamHandler(sys.stdout)]

    logging.basicConfig(
        level=logging.DEBUG,
        format="[%(asctime)s] [%(filename)s:%(lineno)d] [%(levelname)-8s] - %(message)s",
        handlers=handlers,
    )


__initialize_package_logger()
