#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The main area where all reader/writer adapters are stored.

This module basically acts as a Singleton. Anyone can run `add_adapter` to
register some class to this registry and get it again later, using one of its methods.

"""

# IMPORT STANDARD LIBRARIES
import logging
import copy

# IMPORT LOCAL LIBRARIES
from ..adapters.readers import csv_reader
from ..adapters.readers import json_reader
from ..adapters.writers import html_writer
from ..adapters.writers import yaml_writer

READER_KEY = "readers"
WRITER_KEY = "writers"


__ADAPTERS = {READER_KEY: dict(), WRITER_KEY: dict()}
LOGGER = logging.getLogger("data_display.registrar")


def get_all_adapters():
    """Get a copy of all registered adapters.

    This is returned as a copy to avoid the user accidentally changing the
    registrar's contents.

    Returns:
        dict[str, dict[str, `AbstractReader` or `AbstractWriter`]]:
            Every adapter that was found by this package.

    """
    return copy.deepcopy(__ADAPTERS)


def get_all_reader_adapters():
    """dict[str: `AbstractReader`]: The registered adapters for reading data files."""
    return get_all_adapters().get(READER_KEY, [])


def get_all_writer_adapters():
    """dict[str: `AbstractWriter`]: The registered adapters for writing human-readable files."""
    return get_all_adapters().get(WRITER_KEY, [])


def get_reader_adapter(name, default=None):
    """`AbstractReader` or default: Get the adapter for `name` or return `default`."""
    return get_all_reader_adapters().get(name, default)


def get_writer_adapter(name, default=None):
    """`AbstractReader` or default: Get the adapter for `name` or return `default`."""
    return get_all_writer_adapters().get(name, default)


def add_adapter(name, category, adapter):
    """Add the given `adapter` to this global registry.

    Args:
        name (str): The identifier for this adapter.
                    The adapter can be queried using this name, later.
        category (str):
            The type of input that this adapter is allowed to run on.
        adapter (`AbstractReader` or `AbstractWriter`):
            The class to add to this registry.

    Raises:
        ValueError:
            If `category` is not a valid option for this registry.

    """
    if category not in __ADAPTERS.keys():
        raise ValueError(
            'Category "{category}" is not allowed.  Options are "{options}".'
            "".format(category=category, options=sorted(__ADAPTERS.keys()))
        )

    __ADAPTERS[category][name] = adapter


def remove_adapter(name, category):
    """Delete the adapter given `name` of type `category`."""
    try:
        del __ADAPTERS[category][name]
    except KeyError:
        LOGGER.debug('Cannot delete category/name because it does not exist "%s/%s"', category, name)


add_adapter("csv", READER_KEY, csv_reader.Reader)
add_adapter("json", READER_KEY, json_reader.Reader)
add_adapter("yaml", WRITER_KEY, yaml_writer.Writer)
add_adapter("html", WRITER_KEY, html_writer.Writer)
