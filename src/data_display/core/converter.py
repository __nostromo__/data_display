#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""A set of basic functions for finding and converting one format to another."""

# IMPORT STANDARD LIBRARIES
import os
import imp

# IMPORT LOCAL LIBRARIES
from . import registrar


SEARCH_ORDER = tuple(
    os.getenv("DATA_DISPLAY_READER_SEARCH_ORDER", "csv,json").split(",")
)


def _get_ordered_reader_adapters():
    """list[`AbstractReader`]: The order that reader adapters will be automatically searched for."""
    def order_by_environment(item):
        order = list(SEARCH_ORDER)
        order += sorted(registrar.get_all_reader_adapters().keys())

        try:
            return order.index(item[0])
        except ValueError:
            return -1

    return [
        adapter
        for _, adapter in sorted(
            registrar.get_all_reader_adapters().items(), key=order_by_environment
        )
    ]


def _process_adapter_files(paths):
    """Evaluate the given Python files as-is so that the user can add adapters.

    Args:
        paths (list[str]):
            Absolute paths on-disk to Python files that contain extra adapters.

    Raises:
        RuntimeError: If any of the files are missing on-disk or invalid.

    """
    bad_paths = set()

    for path in paths:
        if not os.path.isfile(path):
            bad_paths.add(path)

    if bad_paths:
        raise RuntimeError(
            'Paths "{bad_paths}" are not valid files. Check the spelling of the files '
            "and try again.".format(bad_paths=sorted(bad_paths))
        )

    functions = []
    for path in paths:
        name = os.path.splitext(os.path.basename(path))[0]

        module = imp.load_source("data_display_adapters.{name}".format(name=name), path)

        if hasattr(module, "main"):
            functions.append(module.main)
        else:
            bad_paths.add(path)

    if bad_paths:
        raise RuntimeError(
            'Paths "{bad_paths}" don\'t have a main function.'
            "".format(bad_paths=bad_paths)
        )

    for function in functions:
        function()


def find_reader_adapter(input_object, reader_type=""):
    """Find the object to read `input_object`.

    If `reader_type` is given then just use that. Otherwise, try to "guess" the
    correct adapter for `input_object`.

    Args:
        input_object:
            Some object that will be read from by the found adapter.
            This could be a path to a file on disk, raw data, or whatever you need.
        reader_type (str, optional):
            The name of the registered adapter to use.
            If nothing is given then a compatible format type will be searched
            for, instead. Default: "".

    Returns:
        `data_display.adapters.base_reader.AbstractReader` or NoneType:
            The found reader, if any.

    """
    if reader_type:
        return registrar.get_reader_adapter(reader_type)

    for reader in _get_ordered_reader_adapters():
        if reader.is_supported(input_object):
            return reader

    return None


def convert(
        input_object,
        output_type,
        reader_type="",
        reader_options=None,
        writer_options=None,
        output_location="",
):
    """Change the `input_object` into a format of type `output_type`.

    Args:
        input_object:
            Some object that will be read from by the found adapter.
            This could be a path to a file on disk, raw data, or whatever you need.
        output_type (str):
            The name of a registered writer adapter to convert `input_object` into.
        reader_type (str, optional):
            The name of a registered reader adapter that describes `input_object`.
            If nothing is given then the `reader_type` will be searched for,
            automatically. It's recommended to always provide this value for
            the most consistent results. Default: "".
        reader_options (dict[str], optional):
            This is extra data that you can use to control the behavior of
            the reader adapter. Default is None.
        writer_options (dict[str], optional):
            This is extra data that you can use to control the behavior of
            the writer adapter. Default is None.
        output_location (str, optional):
            If the writer-adapter needs to write to the disk then this path
            is where the file will be written to. Default: "".

    Raises
        RuntimeError:
            If `output_location` is missing and `output_type`
            uses an adapter that needs it. This also can raise if
            `output_location` points to a non-existent folder.
        NotImplementedError:
            If `output_type` or `reader_type `is not in the list of
            registered adapters or if `reader_type` could not be found.

    """
    if not reader_options:
        reader_options = None

    if not writer_options:
        writer_options = None

    root = os.path.dirname(output_location)
    if output_location and not os.path.isdir(root):
        raise RuntimeError(
            'The parent directory of output location "{output_location}" does not exist.'
            ''.format(output_location=output_location))

    writer = registrar.get_writer_adapter(output_type)

    if not writer:
        raise NotImplementedError(
            'Output type "{output_type}" is not allowed. Options were, "{options}".'
            "".format(
                output_type=output_type,
                options=sorted(registrar.get_all_writer_adapters().keys()),
            )
        )

    if writer.requires_disk:
        writer = writer(output_location, writer_options)
    else:
        writer = writer(writer_options)

    if writer.requires_disk and not writer.get_output_location():
        raise RuntimeError(
            'Adapter "{writer}" writes to disk but no output_location was given.'
            ''.format(writer=writer)
        )

    reader = find_reader_adapter(input_object, reader_type=reader_type)

    if not reader:
        raise NotImplementedError(
            'No valid reader type could be found. Options were, "{options}".'
            "".format(options=sorted(registrar.get_all_reader_adapters().keys()))
        )

    reader = reader(input_object, reader_options)
    lines = reader.deserialize()
    data = writer.prepare(lines)
    writer.write(data)


def process_adapter_files(paths):
    """Evaluate the given Python files as-is so that the user can add adapters.

    Warning:
        This function needs to be stricter. Otherwise, arbitrary Python code
        could be executed.

    Args:
        paths (list[str]):
            Absolute paths on-disk to Python files that contain extra adapters.

    Raises:
        RuntimeError: If any of the files are missing on-disk or invalid.

    """
    all_adapters = registrar.get_all_adapters()

    try:
        _process_adapter_files(paths)
    except RuntimeError:
        # Restore the original adapters, just in case something went wrong there
        for category, adapters in registrar.get_all_adapters().items():
            for name, adapter in adapters.items():
                registrar.remove_adapter(name, category)

        for category, adapters in all_adapters.items():
            for name, adapter in adapters.items():
                registrar.add_adapter(name, category, adapter)

        raise
