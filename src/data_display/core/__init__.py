"""All code that is not meant to be publically accessible to the user.

These modules, functions, and classes can change at any time (assuming that
they aren't part of `api.py`).

"""
