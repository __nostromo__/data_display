#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Check that the CSV reader works correctly."""

# Author Note:
# It's probably not necessary for me to implement adapter-specific tests (at
# the time of writing, there's already a 96% unit test coverage). That said, if
# I were to do it, these are the kinds of tests I would probably implement.

# # IMPORT STANDARD LIBRARIES
# import unittest


# class EdgeCaseInput(unittest.TestCase):

#     '''Test different CSV files and make sure they are parsed correctly.'''

#     def test_empty(self):
#         '''Check that the CSV reader can process an empty CSV.'''
#         raise NotImplementedError()

#     def test_columns_extra(self):
#         '''Check that the CSV reader can process a CSV with unneeded columns.'''
#         raise NotImplementedError()

#     def test_columns_missing(self):
#         '''Check that the CSV reader can process a CSV with less than 3 columns.'''
#         raise NotImplementedError()

#     def test_dynamic_columns(self):
#         '''Check that CSV files with different columns still work.'''
#         raise NotImplementedError()

#     def test_escaped_delimiters(self):
#         '''Check that a CSV with escaped characters is still read correctly.'''
#         raise NotImplementedError()


# class Options(unittest.TestCase):

#     '''Test different sets of optional settings affect csv_reader.Reader.'''

#     def test_clean_off(self):
#         '''Check that no lines are removed if clean is off.'''
#         raise NotImplementedError()

#     def test_clean_on(self):
#         '''Check that clean removes lines correctly.'''
#         raise NotImplementedError()

#     def test_config_specified_columns(self):
#         '''Check that columns that are manually specified are preferred.'''
#         raise NotImplementedError()

#     def test_display_header(self):
#         '''Check that the header is added to the parsed CSV data, if enabled.'''
#         raise NotImplementedError()
