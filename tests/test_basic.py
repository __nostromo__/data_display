#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Check that converting from one type of format to another works."""

# IMPORT STANDARD LIBRARIES
import os
import tempfile
import textwrap
import unittest

# IMPORT THIRD-PARTY LIBRARIES
import bs4
import yaml
from data_display import api
from data_display.core import converter
from data_display.adapters.readers import csv_reader
from data_display.adapters.readers import json_reader

# IMPORT LOCAL LIBRARIES
from . import common


class Convert(common.FileTestCase):

    """Convert from one input format to another output format."""

    def test_csv_to_html(self):
        """Convert a CSV file to a HTML file."""
        output_location = tempfile.NamedTemporaryFile(suffix=".html").name
        reader_options = {"encoding": "utf8"}
        self.files.add(output_location)

        data = textwrap.dedent(
            """\



            --Name|--Phone Number|--Address
            John Doe|(319) 643-1787|708 S. River St. Ada, OK 74820
            Robert'); DROP TABLE Students;--|(867) 235-0177|354 Thompson Dr. O'Connell Way, MA 02169
            Pablo Diego Fernão José ... de la Santísima Trinidad Ruiz y Picasso|(428) 391-9257|213 Bradford Court Kennewick, WA 99337
            |+1-202-555-0125|
            Some Name||
            Some Name||8724 Glendale Drive Mundelein, IL 60060
            |||
            |+44 1632 960838|62 University St. Beltsville, MD 20705
            ||62 University St. Beltsville, MD 20705





            """
        )

        input_object = self.make_temporary_file(data, suffix=".csv")

        api.convert(
            input_object,
            "html",
            reader_options=reader_options,
            output_location=output_location,
        )

        self.assertTrue(os.path.isfile(output_location))

        with open(output_location, "r") as handler:
            data = handler.read()

        parser = bs4.BeautifulSoup(data, "lxml")
        table = parser.body.table

        first_record = table.findChildren("tr")[0]
        first_name = first_record.findChildren("th")[0]
        last_name = first_record.findChildren("th")[1]
        self.assertEqual("John", first_name.text)
        self.assertEqual("Doe", last_name.text)

    def test_csv_to_yaml(self):
        """Convert a CSV file to a YAML file."""
        output_location = tempfile.NamedTemporaryFile(suffix=".yaml").name
        self.files.add(output_location)

        data = textwrap.dedent(
            """\



            --Name|--Phone Number|--Address
            John Doe|(319) 643-1787|708 S. River St. Ada, OK 74820
            Robert'); DROP TABLE Students;--|(867) 235-0177|354 Thompson Dr. O'Connell Way, MA 02169
            Pablo Diego Fernão José ... de la Santísima Trinidad Ruiz y Picasso|(428) 391-9257|213 Bradford Court Kennewick, WA 99337
            |+1-202-555-0125|
            Some Name||
            Some Name||8724 Glendale Drive Mundelein, IL 60060
            |||
            |+44 1632 960838|62 University St. Beltsville, MD 20705
            ||62 University St. Beltsville, MD 20705





            """
        )

        input_object = self.make_temporary_file(data, suffix=".csv")

        api.convert(input_object, "yaml", output_location=output_location)

        self.assertTrue(os.path.isfile(output_location))
        with open(output_location, "r") as serialized_file:
            data = yaml.safe_load(serialized_file)
        self.assertTrue(data and data[0][0] == "John Doe")

    def test_json_to_html(self):
        """Convert a JSON file to a HTML file."""
        output_location = tempfile.NamedTemporaryFile(suffix=".html").name

        self.files.add(output_location)

        data = textwrap.dedent(
            r"""
            [[], [], [], ["--Name", "--Phone Number", "--Address"], ["John Doe", "(319) 643-1787", "708 S. River St. Ada, OK 74820"], ["Robert'); DROP TABLE Students;--", "(867) 235-0177", "354 Thompson Dr. O'Connell Way, MA 02169"], ["Pablo Diego Fernao Jos\u00e9 ... de la Sant\u00edsima Trinidad Ruiz y Picasso", "(428) 391-9257", "213 Bradford Court Kennewick, WA 99337"], ["", "+1-202-555-0125", ""], ["Some Name", "", ""], ["Some Name", "", "8724 Glendale Drive Mundelein, IL 60060"], ["", "", "", ""], ["", "+44 1632 960838", "62 University St. Beltsville, MD 20705"], ["", "", "62 University St. Beltsville, MD 20705"], [], [], [], [], []]
            """
        )

        input_object = self.make_temporary_file(data, suffix=".json")

        api.convert(input_object, "html", output_location=output_location)

        self.assertTrue(os.path.isfile(output_location))

        with open(output_location, "r") as handler:
            data = handler.read()

        parser = bs4.BeautifulSoup(data, "lxml")
        table = parser.body.table

        first_record = table.findChildren("tr")[0]
        first_name = first_record.findChildren("th")[0]
        last_name = first_record.findChildren("th")[1]
        self.assertEqual("John", first_name.text)
        self.assertEqual("Doe", last_name.text)

    def test_json_to_yaml(self):
        """Convert a JSON file to a YAML file."""
        output_location = tempfile.NamedTemporaryFile(suffix=".yaml").name
        self.files.add(output_location)

        data = textwrap.dedent(
            r"""
            [[], [], [], ["--Name", "--Phone Number", "--Address"], ["John Doe", "(319) 643-1787", "708 S. River St. Ada, OK 74820"], ["Robert'); DROP TABLE Students;--", "(867) 235-0177", "354 Thompson Dr. O'Connell Way, MA 02169"], ["Pablo Diego Fern\u00e3o Jos\u00e9 ... de la Sant\u00edsima Trinidad Ruiz y Picasso", "(428) 391-9257", "213 Bradford Court Kennewick, WA 99337"], ["", "+1-202-555-0125", ""], ["Some Name", "", ""], ["Some Name", "", "8724 Glendale Drive Mundelein, IL 60060"], ["", "", "", ""], ["", "+44 1632 960838", "62 University St. Beltsville, MD 20705"], ["", "", "62 University St. Beltsville, MD 20705"], [], [], [], [], []]
            """
        )

        input_object = self.make_temporary_file(data, suffix=".json")

        api.convert(input_object, "yaml", output_location=output_location)

        self.assertTrue(os.path.isfile(output_location))
        with open(output_location, "r") as serialized_file:
            data = yaml.safe_load(serialized_file)
        self.assertTrue(data and data[0][0] == "John Doe")


class ReaderType(unittest.TestCase):

    """Test that the reader adapter type can be automatically found or called."""

    def test_find_automatically(self):
        """Check all supported reader adapters can be automatically found."""
        self.assertEqual(
            csv_reader.Reader, converter.find_reader_adapter("/some/file.csv")
        )
        self.assertEqual(
            json_reader.Reader, converter.find_reader_adapter("/some/file.json")
        )

    def test_use_name(self):
        """Make sure that users can get an adapter by-name, directly."""
        self.assertEqual(
            csv_reader.Reader,
            converter.find_reader_adapter("/some/file.json", reader_type="csv"),
        )

    def test_not_found(self):
        """Make sure an adapter with an unknown name returns nothing."""
        adapter = converter.find_reader_adapter(
            "/some/file.json", reader_type="some_unknown_type"
        )
        self.assertEqual(None, adapter)
