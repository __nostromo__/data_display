#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Any code that needs to be shared across test modules."""

# IMPORT STANDARD LIBRARIES
import os
import tempfile
import unittest


class FileTestCase(unittest.TestCase):

    """A simple unittest class that cleans up temporary files for you."""

    def setUp(self):
        """Create a property for storing files."""
        self.files = set()

    def tearDown(self):
        """Delete all temporary files."""
        known_exceptions = [
            # If the file does not exist or is a directory
            OSError
        ]

        try:
            # If the file is still in-use
            known_exceptions.append(WindowsError)
        except NameError:
            # We aren't on Windows. Just ignore it, in that case
            pass

        for path in self.files:
            try:
                os.remove(path)
            except tuple(known_exceptions):
                pass

    def make_temporary_file(self, code, suffix=""):
        """str: Make a temporary file, write `code` as its contents, and delete it later."""
        with tempfile.NamedTemporaryFile(
            mode="w", delete=False, suffix=suffix
        ) as handler:
            handler.write(code)

        self.files.add(handler.name)
        return handler.name
