#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Test different methods and features of this package's registrar."""

# IMPORT STANDARD LIBRARIES
import os
import tempfile
import textwrap
import unittest

# IMPORT THIRD-PARTY LIBRARIES
from data_display import api
from data_display.core import converter
from data_display.core import registrar

# IMPORT LOCAL LIBRARIES
from . import common


class ErrorStates(unittest.TestCase):

    """Any test for "wrong" behavior while working with the registrar."""

    def test_bad_type(self):
        """Raise an error if the user tries to add an adapter of some unknown type."""
        with self.assertRaises(ValueError):
            api.add_adapter("my_adapter", "some_bad_type", object())

    def test_reader_not_found(self):
        """Raise an error if a reader adapter cannot be automatically found."""
        with self.assertRaises(NotImplementedError):
            api.convert(
                "/input/unsupported/file.type",
                "yaml",
                output_location=os.path.join(tempfile.gettempdir(), 'somewhere'),
            )

    def test_writer_not_found(self):
        """Raise an error if a given writer adapter does not exist."""
        with self.assertRaises(NotImplementedError):
            api.convert("/input/unsupported/file.yaml", "not_found_type")


class Functionality(common.FileTestCase):

    """Test specific API methods for the registrar."""

    def setUp(self):
        """Keep track of all of the registered adapters before a test runs."""
        super(Functionality, self).setUp()

        self.adapters = registrar.get_all_adapters()

    def tearDown(self):
        """Restore the old adapters after any test completes."""
        super(Functionality, self).tearDown()

        for category, adapters in registrar.get_all_adapters().items():
            for name, adapter in adapters.items():
                api.remove_adapter(name, category)

        for category, adapters in self.adapters.items():
            for name, adapter in adapters.items():
                api.add_adapter(name, category, adapter)

    def test_remove_from_api(self):
        """Check that the user can remove registered adapters using Python."""
        api.remove_adapter("csv", "readers")
        adapters = registrar.get_all_adapters()
        self.assertTrue("csv" not in adapters[registrar.READER_KEY])

    def test_remove_from_file(self):
        """Check that the user can remove registered adapters using a Python file."""
        code = textwrap.dedent(
            """\
            from data_display import api

            def main():
                api.remove_adapter('csv', 'readers')
            """
        )

        path = self.make_temporary_file(code, suffix=".py")
        converter.process_adapter_files([path])

        adapters = registrar.get_all_adapters()
        self.assertTrue("csv" not in adapters[registrar.READER_KEY])

    def test_override_from_api(self):
        """Check that the user can extend existing adapters using Python."""

        class MyClass(object):
            pass

        api.add_adapter("csv", "readers", MyClass)
        self.assertTrue(registrar.get_reader_adapter("csv") == MyClass)

    def test_override_from_file(self):
        """Check that the user can extend existing adapters using a Python file."""
        code = textwrap.dedent(
            """\
            from data_display import api

            class MyClass(object):
                pass

            def main():
                api.add_adapter('csv', 'readers', MyClass)
            """
        )

        path = self.make_temporary_file(code, suffix=".py")
        converter.process_adapter_files([path])

        self.assertTrue(registrar.get_reader_adapter("csv").__name__ == "MyClass")

    def test_override_failure_bad_path(self):
        """If a Python file does not exist then raise an exception."""
        missing_file = tempfile.NamedTemporaryFile(delete=True, suffix=".py").name

        with self.assertRaises(RuntimeError):
            converter.process_adapter_files([missing_file])

    def test_override_failure_missing_function(self):
        """If a Python file has a missing `main()` then raise an exception."""
        code = textwrap.dedent(
            """\
            from data_display import api

            class MyClass(object):
                pass

            api.add_adapter('csv', 'readers', MyClass)
            """
        )

        path = self.make_temporary_file(code, suffix=".py")

        with self.assertRaises(RuntimeError):
            converter.process_adapter_files([path])

    def test_register_from_api(self):
        """Check that the user can add adapters using Python."""

        class MyClass(object):

            """An example class."""

            pass

        api.add_adapter("some_type", "readers", MyClass)
        self.assertTrue(registrar.get_reader_adapter("some_type") == MyClass)

    def test_register_from_file(self):
        """Check that the user can add adapters using a Python file."""
        code = textwrap.dedent(
            """\
            from data_display import api

            class MyClass(object):
                pass

            def main():
                api.add_adapter('some_type', 'readers', MyClass)
            """
        )

        path = self.make_temporary_file(code, suffix=".py")
        converter.process_adapter_files([path])

        self.assertTrue(registrar.get_reader_adapter("some_type").__name__ == "MyClass")
