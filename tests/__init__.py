#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Disable logging and add constructors so that YAML tests pass."""

# IMPORT STANDARD LIBRARIES
import logging
import sys
import os

# IMPORT THIRD-PARTY LIBRARIES
import yaml


__LOGGER = logging.getLogger("data_display")
if os.getenv("DATA_DISPLAY_DISABLE_LOGGER", "1") == "1":
    __LOGGER.setLevel(sys.maxsize)


def pass_return_constructor(loader, node):
    """Get the node's value and return it."""
    return node.value


yaml.SafeLoader.add_constructor("tag:yaml.org,2002:python/str", pass_return_constructor)
yaml.SafeLoader.add_constructor(
    "tag:yaml.org,2002:python/unicode", pass_return_constructor
)
