This file has two purposes:

1. To touch on how this repository satisfies the directions given in `PROJECT_BRIEF.md`
2. To explain the overall design of the repository and any problems / pitfalls
   that were encountered and how they were overcome.


# Addressing points in the project brief
## Project requirements
To briefly reiterate the needs of the project, here's a concise list:

Take some sets of personal data (name, address, phone number) and
serialise them/deserialise them in at least 2 formats, and display it in
at least 2 different ways.

 - In my case, I chose:
  - csv -> html
  - csv -> yaml
  - json -> html
  - json -> yaml

These are the required features for those formats:

- Support for additional storage formats
- Get the list of currently supported formats
- Be able to supply an alternative reader/writer


### Support for additional storage formats
One way to do this is to create a new Python file, add a new adapter
class using `api.add_adapter` and then call `api.convert` to mimic how
the command-line works.

Or you can add the adapter and execute it directly using
`data_display`'s command-line interface.

#### Support for additional storage formats - Using the command-line interface
Create a file such as `~/my_adapter.py`. Next, create a class and have
it inherit from `data_display.api.AbstractReader` or any other class
that inherits it.


`$ cat ~/my_adapter.py`
```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""A module which will extend `data_display` with a new adapter."""

# IMPORT THIRD-PARTY LIBRARIES
import yaml
from data_display import api


class Reader(api.AbstractReader):

    """A converter which reads a YAML file and returns Python objects."""

    description = "Read lines from a YAML file and return them."

    @classmethod
    def is_supported(cls, obj):
        """bool: Check if the given file path points to a CSV file."""
        return obj.lower().endswith(".yaml")

    def deserialize(self):
        """list[list[str]]: Get every line of the stored YAML file."""
        with open(self.handle, 'r') as handler:
            return yaml.load(handler)


def main():
    """Add the custom `Reader` class to `data_display`."""
    api.add_adapter('yaml', 'readers', Reader)
```

Note: `yaml.load` was used to keep the code concise. In a production scenario,
`yaml.safe_load` would be the better alternative.


Now just include `~/my_adapter.py` into the command-line.

```bash
python -m data_display --adapter-files ~/my_adapter.py convert {root}/example/output.yaml html --output-location /tmp/location.html
```

The new YAML adapter will be used to generate a HTML file.

You can also query the format using `ls`.
```bash
python -m data_display --adapter-files ~/my_adapter.py ls
```


### Get the list of currently supported formats
This can be done using the `ls` command.

```bash
python -m data_display ls
```

Example output:

```
# All adapters
## Readers
 - csv - Read files that are separated by a character (like "|") and newlines
 - json - Convert Javascript Object Notation files into Python objects
## Writers
 - html - Write a list of names, addresses, and phone numbers as a webpage table. Add "output_location" as an option to write to disk.
 - yaml - Write a list of names, addresses, and phone numbers to a YAML file. Add "output_location" as an option to write to disk.
```


### Be able to supply an alternative reader/writer
To add this feature, I re-used the same process that was used for
[Support for additional storage formats](#Support-for-additional-storage-formats).

The only difference is, instead of adding a new adapter under a new
name, just give the new adapter the name that already exists.

Here's a quick example of you could extend or overwrite an existing adapter:

`~/my_custom_html_writer.py`
```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# IMPORT THIRD-PARTY LIBRARIES
from data_display import api
from data_display.adapters.writers import html_writer


class Writer(html_writer.Writer):
    def get_content_footer(self, lines):
        """list[str]: Close the tags that were created in `get_content_header`."""
		footer = super(Writer, self).get_content_footer(lines)
		return ["Ho ho ho!"] + footer

    def get_modified_line(self, line):
        new_line = ["Santa Claus", "1-800-555-5555", "North Pole"]
        return super(Writer, self).get_modified_line(new_line)


def main():
    """Add the custom `Reader` class to `data_display`."""
    api.add_adapter('html', 'writers', Writer)
```


Now just call it as normal:

```bash
python -m data_display --adapter-files ~/my_custom_html_writer.py convert {root}/example/input.csv html --output-location /tmp/location.html
```


# The encountered problems/pitfalls and how they were overcome
## Anticipated issues
### Encoding and Python 2
The first issue that I anticipated needing to deal with is encoding.
Python 3 uses strings and bytes to read/write text but Python 2 uses
strings, bytes, and unicode. So if text contains utf8 or latin1 symbols,
it causes encoding/decoding errors for our handlers.

Thankfully, the solution is simple. I added a flag to the command-line
interface to let the user specify an encoding when the input file is
first read.

To use it, all you have to do is include `data_display convert input.csv
output_type --reader-options '{"encoding": "utf8"}'`. Then the encoding
will be handled and there is no issues on-write.


### To output or not to output, that is the question
When writing the flags for the command-line interface, I had to decide
if I was going to add a flag for an output file location and, if so,
should it be required. Adapters don't need to write to disk (example:
an adapter could print to screen or write to a database) so adding an
output location as a required flag isn't possible. At first, I decided
to add "output_location" as a key/value to the `--reader-options` flag
but there are problems with that solution.

- Adding it as a key to `--writer-options` means that the output location will
  be added to the metadata of a writer adapter. If someone tried to write a new
  adapter, they may not know to look there for the output location.  It's not
  obvious.
- Since the output location as metadata, we may not know until the `write()`
  method is called that the output location wasn't given. And that's almost the
  very last step of the program. We need a way of checking that at the start
- In my opinion, the syntax is bad. For example:

```bash
python -m data_display input.csv html --writer-options '{"output_location": "/tmp/location.html"}'
```

The solution was to make an optional flag called `--output-location` in
the command-line interface. Then, change the existing writer adapter
classes and add a new property called `requires_disk`. I then changed
the `convert()` function so that, if `requires_disk` is True and no
output location was found from the user's input then flag an error.

Now, instead of writing this:


```bash
python -m data_display input.csv html --writer-options '{"output_location": "/tmp/location.html"}'
```

You can write this:

```bash
python -m data_display input.csv html --output-location /tmp/location.html
```

The user's output location is now checked early in the program, before
any classes are instantiated. Future adapters which don't need to write
to disk aren't tied to the same interface as those that do. The format
types that do write to disk now have a concise, clear way of writing an
output path. Excellent!


## Problems/Pitfalls
The input file for this package assumes 3 columns of personal data: The user's
name, phone number, and address. But if those columns come in the wrong order,
it can caues issues whenever the format is written to disk. This came up when I
was writing the HTML adapter. I had decided to output 4 columns instead of 3
(just to show that you can do interesting things with the data before writing
to disk) by splitting the user's name column into first name and last name
columns. One of my test input files listed phone numbers first however so the
area code became the first name and the rest of the number became the last
name.

To fix this, I added two things:

- The built-in reader adapters were changed to "search" for the correct columns
  for each item. If does this by searching for a "header" line that looks like
  this: `["--Name", "--Phone Number", "--Address"]`. I could have also used
  regex or some other engine for partial matching but this seemed good for this
  exercise. The columns are found by searching for each index in the header and
  used whenever the user writes to-disk.
- If the input file does not have a header, the user also has the option to
  specify the columns, directly. To make that possible, I added 3 keys to
  `--reader-options`: address_column_index, name_column_index, and
  phone_column_index. These keys will override any automatically-found column
  indexes.


## How design patterns helped create this tool
### Singleton registrar
To register and store the different adapter classes, I used a Singleton by using
the entire [registrar.py file](src/data_display/core/registrar.py)
There are other implementations of the Singleton pattern in Python such as the
[metaclass method](https://python-3-patterns-idioms-test.readthedocs.io/en/latest/Singleton.html).
I think those implementations are a great way to introduce people to
meta-programming but, in my case, a protected dict and a few functions is
perfect.


### Abstract Factory adapters
If other developers add or override the adapters in this package, I need
a way to encourage others to write classes that won't break. The way I
chose to do this is with abstract interfaces for the reader and write
adapters.

Using [base_reader.py](src/data_display/adapters/base_reader.py)
and [base_writer.py](src/data_display/adapters/base_writer.py),
I am able make changes to the interface of the adapters and be sure that those
requirements are upheld by the subclasses that inherit from them.


### Chain Of Responsiblity searcher
The user doesn't have to specify the type of file that they're trying to
parse and read from. The reason why is because each reader adapter has a
method called `is_supported()` which it uses to check if the input file
can be parsed by that class.

As of writing, if the user doesn't add an explicit reader type, I
made each class "guess" if it can handle the work by running their
`is_supported()` method in a for-loop.

Originally though, I had planned to accomplish this using the Chain of
Responsibility pattern. The idea was to have an object try to handle the
file and, if it couldn't, to then pass the file along to another object
until a valid adapter was found.

I realized though that I wanted to customize the order that each adapter
is checked. To get that effect in Chain Of Responsibilty wouldn't be too
difficult but the code would have been a bit complex. While thinking of
solutions, I decided that adding `is_supported()` would be a simpler
approach. The cost of that decision was every reader adapter would have
to implement that method but that requirement was acceptable to me.

The end result is located in [converter.py](src/data_display/core/converter.py)

Now, users can control the search order using the `DATA_DISPLAY_READER_SEARCH_ORDER`
environment variable.
